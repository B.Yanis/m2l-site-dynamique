<?php
//require_once 'modele\dao\contratDAO.php';
//require_once 'modele\dto\contrats.php';

$_SESSION['listeContrats'] = new Contrats(ContratDAO::lesContrats($_SESSION['identification']->getIDUser()));



if(isset($_GET['contrat'])){
	$_SESSION['contrat']= $_GET['contrat'];
}
else
{
	if(!isset($_SESSION['contrat'])){
		$_SESSION['contrat']="0";
	}
}

$menuContrat = new Menu("menuContrat");


foreach ($_SESSION['listeContrats']->getContrats() as $unContrat){
	$menuContrat->ajouterComposant($menuContrat->creerItemLien($unContrat->getidContrat() ,$unContrat->getDateDebut()));
}

$leMenuContrat = $menuContrat->creerMenu($_SESSION['contrat'], "contrat");

//-----------------------------------------------------------------------------------

$contratActif = $_SESSION['listeContrats']->chercheContrat($_SESSION['contrat']);
echo'<br><br>';


$formContrat = new Formulaire("post", "index.php" , "formulaireContrat","formulaireContrat");

if($_SESSION['contrat']!="0"){


    $formContrat->ajouterComposantLigne($formContrat->creerLabel("Date de Début : " , "labelcontrat") , 1 );
    $formContrat->ajouterComposantLigne($formContrat->creerInputTexte("dateDebut", "dateDebut", $contratActif->getDateDebut() , "1", "1", "1") , 1 );
    $formContrat->ajouterComposantTab();

	$formContrat->ajouterComposantLigne($formContrat->creerLabel("Date de Fin : " , "labelcontrat") , 1 );
    $formContrat->ajouterComposantLigne($formContrat->creerInputTexte("dateFin", "dateFin", $contratActif->getDateFin() , "1", "", "1") , 1 );
    $formContrat->ajouterComposantTab();

	$formContrat->ajouterComposantLigne($formContrat->creerLabel("Type de Contrat : " , "labelcontrat") , 1 );
    $formContrat->ajouterComposantLigne($formContrat->creerInputTexte("typeContrat", "typeContrat", $contratActif->getTypeContrat() , "1", "", "1") , 1 );
    $formContrat->ajouterComposantTab();

	$formContrat->ajouterComposantLigne($formContrat->creerLabel("Nombre d'heure : " , "labelcontrat") , 1 );
    $formContrat->ajouterComposantLigne($formContrat->creerInputTexte("nbHeure", "nbHeure", $contratActif->getnbHeure() , "1", "", "1") , 1 );
    $formContrat->ajouterComposantTab();
       
}

$formContrat->creerFormulaire();


require_once 'vue/vueContrat.php' ;
