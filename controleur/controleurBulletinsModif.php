<?php

//$_SESSION['listeUtilisateur'] = new utilisateurs(UtilisateurDAO::allSalarié());
$_SESSION['listeUtilisateur'] = new utilisateurs(UtilisateurDAO::allUsers());

//----------------------------------------------------------------------------

if(isset($_GET['bulletinsModifs'])){
	$_SESSION['bulletinsModifs']= $_GET['bulletinsModifs'];
}
else
{
	if(!isset($_SESSION['bulletinsModifs'])){
		$_SESSION['bulletinsModifs']="0";
	}
}





//----------------------------------------------------------------------------

$menuUtilisateur = new Menu("menuBulletinModif");


foreach ($_SESSION['listeUtilisateur']->getUsers() as $unUtilisateur){
	$menuUtilisateur->ajouterComposant($menuUtilisateur->creerItemLien($unUtilisateur->getIDUser() ,$unUtilisateur->getPrenom()));
}

$leMenuUtilisateur = $menuUtilisateur->creerMenu($_SESSION['bulletinsModifs'], "bulletinsModifs");

//--------------------------------------------------------------------------------------

$utilisateurActif = $_SESSION['listeUtilisateur']->chercheUsers($_SESSION['bulletinsModifs']);

$_SESSION['utilisateurActif'] = $utilisateurActif;


//---------------------------------------------------------------------------------------

$_SESSION['listeContrats'] = new contrats(contratDAO::allContrats());

$contratActif = $_SESSION['listeContrats']->chercheContrat($_SESSION['bulletinsModifs']);

$_SESSION['listeContrats'] = new Contrats(ContratDAO::lesContrats($_SESSION['bulletinsModifs']));



//var_dump($_SESSION['listeContrats']);

//--------------------------------------------------------------------------------------
$_SESSION['listeBulletins'] = new bulletins(BulletinDAO::allBulletin());

$bulletinActif = $_SESSION['listeBulletins']->chercheBulletin($_SESSION['bulletinsModifs']);




//---------------------------------------------------------------------------------------

$formContrat = new Formulaire("post", "index.php" , "formulaireContrat","formulaireContrat");

$i = 0;
$j= 0;
if($_SESSION['listeContrats']!="0"){ 
    if($_SESSION['bulletinsModifs']!= 0){
        $formContrat->ajouterComposantLigne("<p>---------------------------------------------------------------------------</p>");
        $formContrat->ajouterComposantLigne("Ajouter Nouveau Contrat : ");
        $formContrat->ajouterComposantLigne("</br>");
        $formContrat->ajouterComposantLigne("</br>");
        $formContrat->ajouterComposantLigne($formContrat->creerLabel("Date de Début : " ));
        $formContrat->ajouterComposantLigne($formContrat->creerInputTexte("dateDebutAjout", "dateDebutAjout",  "", "", "", ""));
        $formContrat->ajouterComposantTab();

        $formContrat->ajouterComposantLigne($formContrat->creerLabel("Date de Fin : " ));
        $formContrat->ajouterComposantLigne($formContrat->creerInputTexte("dateFinAjout", "dateFinAjout",  "", "", "", ""));
        $formContrat->ajouterComposantTab();

        $formContrat->ajouterComposantLigne($formContrat->creerLabel("Type de contrat : " ));
        $formContrat->ajouterComposantLigne($formContrat->creerInputTexte("typeContratAjout", "typeContratAjout",  "", "", "", ""));
        $formContrat->ajouterComposantTab();

        $formContrat->ajouterComposantLigne($formContrat->creerLabel("Nombre d'heure : "));
        $formContrat->ajouterComposantLigne($formContrat->creerInputTexte("nbHeureAjout", "nbHeureAjout",  "", "", "", ""));
        $formContrat->ajouterComposantTab();
        $formContrat->ajouterComposantLigne("</br>");
        $formContrat->ajouterComposantLigne($formContrat->creerInputSubmit("Ajouter" , "Ajouter", "Ajouter Contrat"));
        $formContrat->ajouterComposantTab();  


        foreach ($_SESSION['listeContrats']->getContrats() as $unContrat ){
            $i = $i + 1;
            $formContrat->ajouterComposantLigne("</br>");
            $formContrat->ajouterComposantLigne("Contrat : ");
            $formContrat->ajouterComposantLigne("</br>");
            $formContrat->ajouterComposantLigne("</br>");
            $formContrat->ajouterComposantLigne($formContrat->creerLabel("idContrat : "));
            $formContrat->ajouterComposantLigne($formContrat->creerInputTexte("idContrat", "idContrat", $unContrat->getIDContrat() , "", "", ""));
            $formContrat->ajouterComposantLigne("</br>");
            $formContrat->ajouterComposantLigne($formContrat->creerLabel("DateDebut : "));
            $formContrat->ajouterComposantLigne($formContrat->creerInputTexte("dateDebut".$i, "dateDebut".$i, $unContrat->getDateDebut() , "", "", ""));
            $formContrat->ajouterComposantLigne("</br>");
            $formContrat->ajouterComposantLigne($formContrat->creerLabel("DateFin : "));
            $formContrat->ajouterComposantLigne($formContrat->creerInputTexte("dateFin".$i, "dateFin".$i, $unContrat->getDateFin() , "", "", ""));
            $formContrat->ajouterComposantLigne("</br>");
            $formContrat->ajouterComposantLigne($formContrat->creerLabel("TypeContrat : "));
            $formContrat->ajouterComposantLigne($formContrat->creerInputTexte("typeContrat".$i, "typeContrat".$i, $unContrat->getTypeContrat() , "", "", ""));
            $formContrat->ajouterComposantLigne("</br>");
            $formContrat->ajouterComposantLigne($formContrat->creerLabel("Nombre d'heure : " ));
            $formContrat->ajouterComposantLigne($formContrat->creerInputTexte("nbheure".$i, "nbheure".$i, $unContrat->getNbHeure() , "", "", ""));
            $formContrat->ajouterComposantLigne("</br>");
            $formContrat->ajouterComposantLigne("</br>");     
            $formContrat->ajouterComposantLigne($formContrat->creerInputSubmit("Modifier".$i, "Modifier".$i, "Modifier Contrat"));
            $formContrat->ajouterComposantLigne($formContrat->creerInputSubmit("Supprimer".$i , "Supprimer".$i, "Supprimer Contrat"));
            $formContrat->ajouterComposantLigne("</br>");
            $formContrat->ajouterComposantLigne("</br>");
            $formContrat->ajouterComposantLigne("Bulletin : ");
            $formContrat->ajouterComposantLigne("</br>");
            $formContrat->ajouterComposantTab();
            $formContrat->ajouterComposantLigne("<p>---------------------------------------------------------------------------</p>");
            $formContrat->ajouterComposantLigne("Ajouter Nouveau Bulletin : ");
            $formContrat->ajouterComposantLigne("</br>");
            $formContrat->ajouterComposantLigne("</br>");
            $formContrat->ajouterComposantLigne($formContrat->creerLabel("Mois : " ));
            $formContrat->ajouterComposantLigne($formContrat->creerInputTexte("moisAjout".$i, "moisAjout".$i,  "", "", "", ""));
            $formContrat->ajouterComposantTab();
            $formContrat->ajouterComposantLigne($formContrat->creerLabel("Annee : " ));
            $formContrat->ajouterComposantLigne($formContrat->creerInputTexte("anneeAjout".$i, "anneeAjout".$i,  "", "", "", ""));
            $formContrat->ajouterComposantTab();
            $formContrat->ajouterComposantLigne($formContrat->creerLabel("Bulletin PDF : " ));
            $formContrat->ajouterComposantLigne($formContrat->creerInputTexte("pdfAjout".$i, "pdfAjout".$i,  "", "", "", ""));
            $formContrat->ajouterComposantTab();
            $formContrat->ajouterComposantTab();
            $formContrat->ajouterComposantLigne("</br>");
            $formContrat->ajouterComposantLigne($formContrat->creerInputSubmit("AjouterBulletin".$i , "AjouterBulletin".$i, "Ajouter Bulletin"));
            
            $formContrat->ajouterComposantTab();  
            
            $_SESSION['listeBulletins'] = new Bulletins(BulletinDAO::lesBulletins($unContrat->getIDContrat()));

            foreach ($_SESSION['listeBulletins']->getBulletins() as $unBulletin){
                $j = $j + 1;    
                if($unBulletin->getIdContrat() == $unContrat->getIDContrat()){
                    $formContrat->ajouterComposantLigne("</br>");
                    $formContrat->ajouterComposantLigne($formContrat->creerLabel("idBulletin : "));
                    $formContrat->ajouterComposantLigne($formContrat->creerInputTexte("idBulletin".$j, "idBulletin".$j, $unBulletin->getIdBulletin() , "", "", ""));
                    $formContrat->ajouterComposantLigne("</br>");
                    $formContrat->ajouterComposantLigne($formContrat->creerLabel("Mois : "));
                    $formContrat->ajouterComposantLigne($formContrat->creerInputTexte("mois".$j, "mois".$j, $unBulletin->getMois()  , "", "", ""));
                    $formContrat->ajouterComposantLigne("</br>");
                    $formContrat->ajouterComposantLigne($formContrat->creerLabel("Annee : "));
                    $formContrat->ajouterComposantLigne($formContrat->creerInputTexte("annee".$j, "annee".$j, $unBulletin->getAnnee()  , "", "", ""));
                    $formContrat->ajouterComposantLigne("</br>");
                    $formContrat->ajouterComposantLigne($formContrat->creerLabel("PDF : "));
                    $formContrat->ajouterComposantLigne($formContrat->creerInputTexte("pdf".$j, "pdf".$j, $unBulletin->getBulletinPDF()  , "", "", ""));
                    $formContrat->ajouterComposantLigne($formContrat->creerInputSubmit("ModifierBulletin".$j, "ModifierBulletin".$j, "Modifier Contrat"));
                    $formContrat->ajouterComposantLigne($formContrat->creerInputSubmit("SupprimerBulletin".$j , "SupprimerBulletin".$j, "Supprimer Contrat"));
                    $formContrat->ajouterComposantTab();


                    if(isset($_POST['ModifierBulletin'.$j])){
                        $idBulletin = $unBulletin->getIdBulletin();
                        //var_dump($_POST['idBulletin'.$i], $_POST['mois'.$i], $_POST['annee'.$i], $_POST['pdf'.$i]);
                        $reponseSGBD = BulletinDAO::updateBulletin($idBulletin, $_POST['mois'.$j], $_POST['annee'.$j], $_POST['pdf'.$j]);
                        if($reponseSGBD){
                            echo "Le contrat a été supprimé avec succès.";
                        } else {
                            echo "La suppression du contrat a échoué.";
                        }
                    }
                }
                    
                if(isset($_POST['SupprimerBulletin'.$j])){
                    $idBulletin = $unBulletin->getIdBulletin();
                    
                    $suppressionReussie = BulletinDAO::deleteBulletin($idBulletin);
                    
                    if($suppressionReussie){
                        
    
                    } else {
                        echo "La suppression du contrat a échoué.";
                    }
                }
                
                
            
            }
            $formContrat->ajouterComposantLigne("</br>");
            $formContrat->ajouterComposantLigne("<p>---------------------------------------------------------------------------</p>");

            if(isset($_POST['Supprimer'.$i])){
                $idContrat = $unContrat->getIDContrat();
                
                $suppressionReussie = ContratDAO::deleteContrat($idContrat);

                
                if($suppressionReussie){
                    

                } else {
                    echo "La suppression du contrat a échoué.";
                }
            }

            if(isset($_POST['Modifier'.$i])){
                $idContrat = $unContrat->getIDContrat();
                $reponseSGBD = ContratDAO::updateContrat($idContrat, $_POST['dateDebut'.$i], $_POST['dateFin'.$i], $_POST['typeContrat'.$i], $_POST['nbheure'.$i]);
                if($reponseSGBD){
                    echo "Le contrat a été supprimé avec succès.";
                } else {
                    echo "La suppression du contrat a échoué.";
                }
            }

            if(isset($_POST['AjouterBulletin'.$i])){
                $reponseSGBD = BulletinDAO::createBulletin($unContrat->getIDContrat() ,$_POST['moisAjout'.$i], $_POST['anneeAjout'.$i], $_POST['pdfAjout'.$i]);

                if($reponseSGBD){
                    echo "Le contrat a été supprimé avec succès.";
                } else {
                    echo "La suppression du contrat a échoué.";
                }

            }

            
        }
    }
}



if(isset($_POST['Ajouter'])){
    $reponseSGBD = ContratDAO::createContrat($lastId, $utilisateurActif->getIDUser(), $_POST['dateDebutAjout'], $_POST['dateFinAjout'], $_POST['typeContratAjout'], $_POST['nbHeureAjout']);
    
    if($reponseSGBD){
        $_SESSION['equipe']=$reponseSGBD;
    }
}




$formContrat->creerFormulaire();

require_once 'vue/vueBulletinModif.php' ;
