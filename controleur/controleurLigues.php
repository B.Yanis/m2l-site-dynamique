<?php
require_once 'modele/dto/ligue.php' ;
include_once 'modele/dao/LigueDAO.php';

$listeLigues = new Ligues(LigueDAO::getAll());


$ligue_HTML='<div class="liste_ligues"><?php function modifLigue($idLigue){
	echo $idLigue;
}?>';
$nav = '<nav class="sidenav"><h3 class="titreListe">Les ligues</h3><ul>';
foreach ($listeLigues->getLigues() as $ligue){
	$idLigue=$ligue->getIDLIGUE();
    $nom=$ligue->getNOM();
    $site=$ligue->getSITE();
    $desc=$ligue->getDESCRIPTIF();

    $ligue_HTML.='<div class="article"><a class="anchor" id="'.$nom.'"></a><h3 class="titre">Ligue de '.$nom.'</h3><p class="corps">'.$desc." <br><br> Vous Trouverez plus d'informations sur leur <a href='".$site."'>site internet</a></p></div><p>Clubs affiliés : </p>";
    
    $listeClubs=ClubDAO::getClubs($idLigue);
    $i=0;
    
    foreach ($listeClubs as $club){
        if ($club->getIDLigue() == $idLigue){
            $idCommune = $club->getIDCOMMUNE();
            $position = ClubDAO::getPosition($idCommune); 
            $ligue_HTML.='<label for="'.$nom.$i.'"><input type="checkbox" name="menu" id="'.$nom.$i.'"><div class="listClub"><h1>'.$club->getNOMCLUB()."</h1><br><p>".
            $club->getADRESSECLUB().
            "<br>".
            $position->getCODEPOSTAL().
            "<br>".
            $position->getNOMCOMMUNE().
            "</p></div></label>";
            $i++;
    }
}


    $ligue_HTML.= '</div>';
        
    $nav.='<li><a href="#'.$nom.'">'.$nom.'</a></li>';
}    
$ligue_HTML.="</div>";
$nav.='</ul></nav>';

function supprimerLigue($idLigue){ 
    LigueDAO::supprimerLigue( $idLigue ); 
}
include_once('vue/vueLigues.php');



?>