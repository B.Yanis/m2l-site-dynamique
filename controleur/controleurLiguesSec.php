<?php
$formLigue = new Formulaire("post","index.php","formLigue","formLigue");
/*****************************************************************************************************
* Instancier un objet contenant la liste des ligues et le conserver dans une variable de session
*****************************************************************************************************/
$_SESSION['listeLigues'] = new Ligues(LigueDAO::getAll());

	
/*****************************************************************************************************
 * Conserver dans une variable de session l'item actif du menu ligue
 *****************************************************************************************************/
if(isset($_GET['ligue'])){
	$_SESSION['ligue']= $_GET['ligue'] ;
}
else
{
	if(!isset($_SESSION['ligue'])){
		$_SESSION['ligue']="0";
	}
}



/*****************************************************************************************************
 * Créer un menu vertical à partir de la liste des ligues
 *****************************************************************************************************/
$menuLigue = new Menu("menuLigue");


foreach ($_SESSION['listeLigues']->getLigues() as $uneLigue){
	$menuLigue->ajouterComposant($menuLigue->creerItemLien($uneLigue->getNOM() ,$uneLigue->getIDLIGUE()));
}

$leMenuLigues = $menuLigue->creerMenuLigue($_SESSION['ligue']);

/*****************************************************************************************************
 * Récupérer la ligue sélectionnée
 *****************************************************************************************************/
$ligueActive = $_SESSION['listeLigues']->chercheLigueNom($_SESSION['ligue']);

if (isset($_POST['Modifier'])){
	$id=$ligueActive->getIDLIGUE();



	//nom ligue
	$composant = $formLigue->creerLabel("Nom Ligue : ", "labelLigue".$id);
	$formLigue->ajouterComposantLigne($composant,1);

	$composant = $formLigue->creerInputTexteV("nomLigue","nomLigue", $ligueActive->getNOM(),"1","","");
	$formLigue->ajouterComposantLigne($composant,1);
	$formLigue->ajouterComposantTab();

	//site ligue
	$composant = $formLigue->creerLabel("Site Ligue : ", "labelLigue".$id);
	$formLigue->ajouterComposantLigne($composant,1);

	$composant = $formLigue->creerInputTexteV("siteLigue","siteLigue", $ligueActive->getSITE(),"1","","");
	$formLigue->ajouterComposantLigne($composant,1);
	$formLigue->ajouterComposantTab();

	//desccriptif ligue
	$composant = $formLigue->creerLabel("Descriptif Ligue : ", "labelLigue".$id);
	$formLigue->ajouterComposantLigne($composant,1);

	$composant = $formLigue->creerInputTexteV("descriptifLigue","descriptifLigue", $ligueActive->getDESCRIPTIF(),"1","","");
	$formLigue->ajouterComposantLigne($composant,1);
	$formLigue->ajouterComposantTab();

    $composant = $formLigue->creerInputSubmit("Annuler", "Annuler", "Annuler");
    $formLigue->ajouterComposantLigne($composant,1);
    $composant = $formLigue->creerInputSubmit("ModifierC", "ModifierC", "Confirmer");
    $formLigue->ajouterComposantLigne($composant,1);
    $formLigue->ajouterComposantTab();

    $formLigue->creerFormulaire();

}
elseif (isset($_POST['Ajouter'])){

	//nom ligue
	$composant = $formLigue->creerLabel("Nom Ligue : ", "labelLigue");
	$formLigue->ajouterComposantLigne($composant,1);

	$composant = $formLigue->creerInputTexteV("nomLigue","nomLigue", "","1","","");
	$formLigue->ajouterComposantLigne($composant,1);
	$formLigue->ajouterComposantTab();

	//site ligue
	$composant = $formLigue->creerLabel("Site Ligue : ", "labelLigue");
	$formLigue->ajouterComposantLigne($composant,1);

	$composant = $formLigue->creerInputTexteV("siteLigue","siteLigue", "","1","","");
	$formLigue->ajouterComposantLigne($composant,1);
	$formLigue->ajouterComposantTab();

	//desccriptif ligue
	$composant = $formLigue->creerLabel("Descriptif Ligue : ", "labelLigue");
	$formLigue->ajouterComposantLigne($composant,1);

	$composant = $formLigue->creerInputTexteV("descriptifLigue","descriptifLigue", "","1","","");
	$formLigue->ajouterComposantLigne($composant,1);
	$formLigue->ajouterComposantTab();

    $composant = $formLigue->creerInputSubmit("Annuler", "Annuler", "Annuler");
    $formLigue->ajouterComposantLigne($composant,1);
    $composant = $formLigue->creerInputSubmit("AjouterConf", "AjouterConf", "AjouterConf");
    $formLigue->ajouterComposantLigne($composant,1);
    $formLigue->ajouterComposantTab();

	$formLigue->creerFormulaire();
}
elseif($_SESSION['ligue'] != 0){
	$id=$ligueActive->getIDLIGUE();

	//nom ligue
	$composant = $formLigue->creerLabel("Nom Ligue : ", "labelLigue".$id);
	$formLigue->ajouterComposantLigne($composant,1);

	$composant = $formLigue->creerInputTexteV("nomLigue","nomLigue", $ligueActive->getNOM(),"1","","1");
	$formLigue->ajouterComposantLigne($composant,1);
	$formLigue->ajouterComposantTab();

	//site ligue
	$composant = $formLigue->creerLabel("Site Ligue : ", "labelLigue".$id);
	$formLigue->ajouterComposantLigne($composant,1);

	$composant = $formLigue->creerInputTexteV("siteLigue","siteLigue", $ligueActive->getSITE(),"1","","1");
	$formLigue->ajouterComposantLigne($composant,1);
	$formLigue->ajouterComposantTab();

	//desccriptif ligue
	$composant = $formLigue->creerLabel("Descriptif Ligue : ", "labelLigue".$id);
	$formLigue->ajouterComposantLigne($composant,1);

	$composant = $formLigue->creerInputTexteV("descriptifLigue","descriptifLigue", $ligueActive->getDESCRIPTIF(),"1","","1");
	$formLigue->ajouterComposantLigne($composant,1);
	$formLigue->ajouterComposantTab();

    $composant = $formLigue->creerInputSubmit("Modifier", "Modifier", "Modifier");
    $formLigue->ajouterComposantLigne($composant,1);
	$composant = $formLigue->creerInputSubmit("Supprimer", "Supprimer", "Supprimer");
    $formLigue->ajouterComposantLigne($composant,1);
	$composant = $formLigue->creerInputSubmit("Ajouter", "Ajouter", "Ajouter");
    $formLigue->ajouterComposantLigne($composant,1);
    $formLigue->ajouterComposantTab();

    $formLigue->creerFormulaire();
	

}
elseif($_SESSION['ligue'] == 0){
	$formLigue->ajouterComposantLigne($formLigue->creerLabel("Sélectionnez une ligue " , "messageLigue") , 1 );
	$formLigue->ajouterComposantTab();	

}

if(isset($_POST['AjouterConf'])){
	$nom = $_POST['nomLigue'];
	$site = $_POST['siteLigue'];
	$descriptif = $_POST['descriptifLigue'];
	$var = new LigueDAO;
	$var->ajouterLigue($nom , $site , $descriptif );
	$var->getAll();
	$_SESSION['ligue']='0';
	header("Refresh:0");
}

if(isset($_POST['ModifierC'])){
	$nom = $_POST['nomLigue'];
	$site = $_POST['siteLigue'];
	$descriptif = $_POST['descriptifLigue'];
	$var = new LigueDAO;
	$var->modifierLigue($id, $nom , $site , $descriptif );
	$var->getAll();
	header("Refresh:0");
	$_SESSION['ligue']='0';
}

if(isset($_POST['Supprimer'])){
	$var = new LigueDAO;
	$var->supprimerLigue($id);
	$var->getAll();
	$_SESSION['ligue']=0;
	header("Refresh:0");
}
$formLigue->creerFormulaire();


include_once('vue/vueLiguesSec.php');


