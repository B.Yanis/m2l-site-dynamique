<div class="conteneur">
	<header>
		<?php include 'haut.php' ;?>
	</header>
	<main>
		<div class='gauche'>
			<?php include 'vue/ligues/liguesGaucheSec.php' ;?>
		</div>
		<div class='droite'>
			<?php include 'vue/ligues/liguesDroiteSec.php' ;?>
		</div>
	</main>
	<footer>
		<?php include 'bas.php' ;?>
	</footer>
</div>