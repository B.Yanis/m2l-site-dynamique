<div class="conteneur">
	<header>
		<?php include 'haut.php' ;?>
	</header>
	<main>
	<div class='gauche'>
			<?php include 'vue/bulletins/vueGaucheBulletin.php' ;?>
		</div>
		<div class='droite'>
			<?php include 'vue/bulletins/vueDroiteBulletin.php' ;?>
		</div>
	</main>
	<footer>
		<?php include 'bas.php' ;?>
	</footer>
</div>