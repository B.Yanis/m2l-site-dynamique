<div class="conteneur">
	<header>
		<?php include 'haut.php' ;?>
	</header>
	<main>
	<div class='gauche'>
			<?php include 'vue/bulletinsModif/vueGaucheBulletinModif.php' ;?>
		</div>
		<div class='droite'>
			<?php include 'vue/bulletinsModif/vueDroiteBulletinModif.php' ;?>
		</div>
	</main>
	<footer>
		<?php include 'bas.php' ;?>
	</footer>
</div>