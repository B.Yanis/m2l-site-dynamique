<?php
class Utilisateurs{
	private array $utilisateurs ;

	public function __construct($array){
		if (is_array($array)) {
			$this->utilisateurs = $array;
		}
	}

	public function getUsers(){
		return $this->utilisateurs;
	}

	public function chercheUsers($unIdUtilisateur){
		$i = 0;
		while ($unIdUtilisateur != $this->utilisateurs[$i]->getIDUser() && $i < count($this->utilisateurs)-1){
			$i++;
		}
		if ($unIdUtilisateur == $this->utilisateurs[$i]->getIDUser()){
			return $this->utilisateurs[$i];
		}
	}
}