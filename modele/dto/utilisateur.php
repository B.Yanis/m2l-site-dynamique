<?php 

class Utilisateur{
    use Hydrate;
    private ?string $idUser;
    private ?string $idLigue;
    private ?string $idFonct;
    private ?string $idClub;
    private ?string $nom;
    private ?string $prenom;
    private ?string $login;
    private ?string $mdp;
    private ?string $statut;



    public function __construct(?string $idUser, ?string $idLigue, ?string $idFonct, ?string $idClub,?string $nom, ?string $prenom, ?string $login, ?string $mdp, ?string $statut){
        $this->idUser = $idUser;
        $this->idLigue = $idLigue;
        $this->idFonct = $idFonct;
        $this->idClub = $idClub;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->login = $login;
        $this->mdp = $mdp;
        $this->statut = $statut;

    }

    public function getIDUser(): string{
        return $this->idUser;
    }

    public function getIDLigue(): string{
        return $this->idLigue;
    }

    public function getIDFonct(): string{
        return $this->idFonct;
    }

    public function getIDClub(): string{
        return $this->idClub;
    }

    public function getNom(): string{
        return $this->nom;
    }

    public function getPrenom(): string{
        return $this->prenom;
    }
    
    public function getLogin(): string{
        return $this->login;
    }

    public function getMdp(): string{
        return $this->mdp;
    }

    public function getStatut(): string{
        return $this->statut;
    }


    public function setIDUser(string $idUser): void{
		$this->idUser = $idUser;
	}	

    public function setIDLigue(string $idLigue): void{
		$this->idLigue = $idLigue;
	}	

    public function setIDFonct(string $idFonct): void{
		$this->idFonct = $idFonct;
	}	

    public function setIDClub(string $idClub): void{
		$this->idClub = $idClub;
	}	

    public function setNom(string $nom): void{
		$this->nom = $nom;
	}	

    public function setPrenom(string $prenom): void{
		$this->prenom = $prenom;
	}	

    public function setLogin(string $login): void{
		$this->login = $login;
	}	

    public function setMdp(string $mdp): void{
		$this->mdp = $mdp;
	}	

    public function setStatut(string $statut): void{
		$this->statut = $statut;
	}	

    public function __toString() {
        return $this->login . ' ' . $this->mdp . ' ' . $this->idFonct;
    }

}

?>