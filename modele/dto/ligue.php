<?php
class Ligue{
    use Hydrate;
	private ?string $IDLIGUE;
	private ?string $Nom;
	private ?string $Site;
	private ?string $Descriptif;

	public function __construct(?string $unIdLigue,?string $unNom, ?string $unSite, ?string $unDescriptif,){
		$this->IDLIGUE = $unIdLigue;
		$this->Nom = $unNom;
		$this->Site = $unSite;
		$this->Descriptif = $unDescriptif;
	}

	public function getIDLIGUE(): string{
		return $this->IDLIGUE;
	}

	public function setIDLIGUE(string $unIdLigue): void{
		$this->IDLIGUE = $unIdLigue;
	}

	public function getNom(): string{
		return $this->Nom;
	}
	
	public function setNom(string $unNom): void{
		$this->Nom = $unNom;
	}

	public function getSite(): string{
		return $this->Site;
	}
	
	public function setSite(string $unSite): void{
		$this->Site = $unSite;
	}

	public function getDescriptif(): string{
		return $this->Descriptif;
	}
	
	public function setDescriptif(string $unDescriptif): void{
		$this->Descriptif = $unDescriptif;
	}	
	public function modifLigue(string $idLigue): void{
		echo $idLigue;
	}
}