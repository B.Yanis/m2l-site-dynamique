<?php

class Contrat{

    use Hydrate;
    private ?int $idContrat;
    private ?string $idUser;
    private ?string $dateDebut;
    private ?string $dateFin;
    private ?string $typeContrat;
    private ?string $nbHeure;

    public function __construct(?int $idContrat, ?string $idUser, ?string $dateDebut, ?string $dateFin, ?string $typeContrat, ?string $nbHeure){

        $this->idContrat = $idContrat;
        $this->idUser = $idUser;
        $this->dateDebut = $dateDebut;
        $this->dateFin = $dateFin;
        $this->typeContrat = $typeContrat;
        $this->nbHeure = $nbHeure;
    }

    public function getidContrat(): int{
        return $this->idContrat;
    }

    public function getidUser(): string{
        return $this->idUser;
    }

    public function getDateDebut(): string{

        if ($this->dateDebut === null) {
            return "Date de début non définie";
        }
        return $this->dateDebut;
    }
    
    public function getDateFin(): string{

        if ($this->dateDebut === null) {
            return "Date de début non définie";
        }
        return $this->dateFin;
    }

    public function getTypeContrat(): string{

        if ($this->dateDebut === null) {
            return "Date de début non définie";
        }
        return $this->typeContrat;
    }

    public function getNbHeure(): string{

        if ($this->dateDebut === null) {
            return "Date de début non définie";
        }
        return $this->nbHeure;
    }

    public function setIdContrat(int $idContrat): void{
		$this->idContrat = $idContrat;
	}	

    public function setIdUser(string $idUser): void{
		$this->idUser = $idUser;
	}	

    public function setDateDebut(string $dateDebut): void{
		$this->dateDebut = $dateDebut;
	}
    
    public function setDateFin(string $dateFin): void{
		$this->dateFin = $dateFin;
	}

    public function setTypeContrat(string $typeContrat): void{
		$this->typeContrat = $typeContrat;
	}

    public function setNbHeure(string $nbHeure): void{
		$this->nbHeure = $nbHeure;
	}
}
