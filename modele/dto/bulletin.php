<?php

class Bulletin{

    use Hydrate;
    private ?int $idBulletin;
    private ?int $idContrat;
    private ?string $mois;
    private ?string $annee;
    private ?string $bulletinPDF;

    public function __construct(?int $idBulletin, ?int $idContrat, ?string $mois, ?string $annee, ?string $bulletinPDF){

        $this->idBulletin = $idBulletin;
        $this->idContrat = $idContrat;
        $this->mois = $mois;
        $this->annee = $annee;
        $this->bulletinPDF = $bulletinPDF;
    }

    public function getIdBulletin(): int{
        return $this->idBulletin;
    }

    public function getIdContrat(): int{
        return $this->idContrat;
    }

    public function getMois(): string{
        return $this->mois;
    }
    
    public function getAnnee(): string{
        return $this->annee;
    }

    public function getBulletinPDF(): string{
        return $this->bulletinPDF;
    }

    public function setIdBulletin(int $idBulletin): void{
		$this->idBulletin = $idBulletin;
	}	

    public function setIdContrat(int $idContrat): void{
		$this->idContrat = $idContrat;
	}	

    public function setMois(string $mois): void{
		$this->mois = $mois;
	}
    
    public function setAnnee(string $annee): void{
		$this->annee = $annee;
	}

    public function setBulletinPDF(string $bulletinPDF): void{
		$this->bulletinPDF = $bulletinPDF;
	}


}