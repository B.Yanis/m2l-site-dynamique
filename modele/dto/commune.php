<?php
class Commune{
    use Hydrate;
	private ?string $IDCOMMUNE;	
	private ?string $CODEDEPARTEMENT;
	private ?string $CODEPOSTAL;	
	private ?string $NOMCOMMUNE;

	public function __construct(?string $IDCOMMUNE, ?string $CODEDEPARTEMENT, ?string $CODEPOSTAL, ?string $NOMCOMMUNE){$this->IDCOMMUNE = $IDCOMMUNE;$this->CODEDEPARTEMENT = $CODEDEPARTEMENT;$this->CODEPOSTAL = $CODEPOSTAL;$this->NOMCOMMUNE = $NOMCOMMUNE;}
	
	public function getIDCOMMUNE(): string {return $this->IDCOMMUNE;}

	public function getCODEDEPARTEMENT(): string {return $this->CODEDEPARTEMENT;}

	public function getCODEPOSTAL(): string {return $this->CODEPOSTAL;}

	public function getNOMCOMMUNE(): string {return $this->NOMCOMMUNE;}

	public function setIDCOMMUNE(string $IDCOMMUNE): void {$this->IDCOMMUNE = $IDCOMMUNE;}

	public function setCODEDEPARTEMENT(string $CODEDEPARTEMENT): void {$this->CODEDEPARTEMENT = $CODEDEPARTEMENT;}

	public function setCODEPOSTAL(string $CODEPOSTAL): void {$this->CODEPOSTAL = $CODEPOSTAL;}

	public function setNOMCOMMUNE(string $NOMCOMMUNE): void {$this->NOMCOMMUNE = $NOMCOMMUNE;}

	

	
	
}
?>	
	
