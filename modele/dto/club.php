<?php
class Club{
    use Hydrate;
	private ?string $IDCLUB;
    private ?string $IDLIGUE;
	private ?string $IDCOMMUNE;
	private ?string $NOMCLUB;
    private ?string $ADRESSECLUB;

    public function __construct(?string $IDCLUB, ?string $IDLIGUE, ?string $IDCOMMUNE, ?string $NOMCLUB, ?string $ADRESSECLUB){
        $this->IDCLUB = $IDCLUB;
        $this->IDLIGUE = $IDLIGUE;
        $this->IDCOMMUNE = $IDCOMMUNE;
        $this->NOMCLUB = $NOMCLUB;
        $this->ADRESSECLUB = $ADRESSECLUB;
    }

    public function getIDCLUB(): string {return $this->IDCLUB;}

	public function getIDLIGUE(): string {return $this->IDLIGUE;}

	public function getIDCOMMUNE(): string {return $this->IDCOMMUNE;}

	public function getNOMCLUB(): string {return $this->NOMCLUB;}

	public function getADRESSECLUB(): string {return $this->ADRESSECLUB;}

	public function setIDCLUB(string $IDCLUB): void {$this->IDCLUB = $IDCLUB;}

	public function setIDLIGUE(string $IDLIGUE): void {$this->IDLIGUE = $IDLIGUE;}

	public function setIDCOMMUNE(string $IDCOMMUNE): void {$this->IDCOMMUNE = $IDCOMMUNE;}

	public function setNOMCLUB(string $NOMCLUB): void {$this->NOMCLUB = $NOMCLUB;}

	public function setADRESSECLUB(string $ADRESSECLUB): void {$this->ADRESSECLUB = $ADRESSECLUB;}

	
	
}
?>