<?php
class LigueDAO{
        
    public static function getAll(){
        
        $result = [];
        $requetePrepa = DBConnex::getInstance()->prepare("select * from LIGUE" );

        $requetePrepa->execute();
        $resultat = $requetePrepa->fetchAll(PDO::FETCH_ASSOC); 
        
        if(!empty($resultat)){
            foreach($resultat as $ligue){
                $uneLigue = new Ligue(null, null,null,null);
                $uneLigue->hydrate($ligue);
                $result[] = $uneLigue;
            }
        }
        return $result;
    }
    public static function getAllFromLigue($idLigue){
        $requetePrepa = DBConnex::getInstance()->prepare("select NOM,SITE,DESCRIPTIF from LIGUE WHERE IDLIGUE = :idLigue");

        $requetePrepa->bindParam(":idLigue", $idLigue);

        $requetePrepa->execute();

        // Récupérez le résultat sous forme d'objet stdClass
        $resultat = $requetePrepa->fetch(PDO::FETCH_ASSOC);

        // Assurez-vous que le résultat n'est pas vide
        if ($resultat) {
            // Récupérez le login et l'idfonct dans des variables distinctes
            return $resultat;
        } else {
            return null; // Aucune correspondance trouvée
        }
    }
    public static function supprimerLigue($idLigue){
        $requetePrepa = DBConnex::getInstance()->prepare("delete from `LIGUE` where IDLIGUE =  :idLigue");

        $requetePrepa->bindParam(":idLigue", $idLigue);

        $requetePrepa->execute();
    }
    public static function getClubs($idLigue){
        $result = [];
        $requetePrepa = DBConnex::getInstance()->prepare("select * from club where idligue = :idLigue" );

        $requetePrepa->bindParam(":idLigue", $idLigue);
    
        $requetePrepa->execute();
        $resultat = $requetePrepa->fetchAll(PDO::FETCH_ASSOC); 
        
        if(!empty($resultat)){
            foreach($resultat as $club){
                $unClub = new Club(null, null,null,null, null);
                $unClub->hydrate($club);
                $result[] = $unClub;
            }
        }
        return $result;
    }
    public static function ajouterLigue($nom,$site,$desc){
        $requetePrepa = DBConnex::getInstance()->prepare("insert into `LIGUE` (`NOM`, `SITE`, `DESCRIPTIF`) VALUES (:nomLigue,:siteLigue,:descLigue)");

        $requetePrepa->bindParam(":nomLigue", $nom);
        $requetePrepa->bindParam(":siteLigue", $site);
        $requetePrepa->bindParam(":descLigue", $desc);

        $requetePrepa->execute();
    }
    public static function modifierLigue($idLigue,$nom,$site,$desc){
        $requetePrepa = DBConnex::getInstance()->prepare("update `ligue` SET `NOM`=:nomLigue,`SITE`=:siteLigue,`DESCRIPTIF`=:descLigue WHERE IDLIGUE= :idLigue");

        $requetePrepa->bindParam(":idLigue", $idLigue);
        $requetePrepa->bindParam(":nomLigue", $nom);
        $requetePrepa->bindParam(":siteLigue", $site);
        $requetePrepa->bindParam(":descLigue", $desc);

        $requetePrepa->execute();
    }

}
?>