<?php
class ContratDAO{
    
    public static function lesContrats($idUser){
        $result = [];
        $requetePrepa = DBConnex::getInstance()->prepare("select * from CONTRAT where IDUSER = :iduser");
       
        $requetePrepa->bindParam(":iduser", $idUser); 
        $requetePrepa->execute();

        $liste = $requetePrepa->fetchAll(PDO::FETCH_ASSOC); 

        if(!empty($liste)){
            foreach($liste as $contrat){
                $unContrat = new Contrat(null,null,null,null,null,null);
                $unContrat->hydrate($contrat);
                $result[] = $unContrat;
            }
        }
        

        return $result;
    }

    public static function allContrats(){
        $result = [];
        $requetePrepa = DBConnex::getInstance()->prepare("select * from CONTRAT");
       
        $requetePrepa->execute();

        $liste = $requetePrepa->fetchAll(PDO::FETCH_ASSOC); 

        if(!empty($liste)){
            foreach($liste as $contrat){
                $unContrat = new Contrat(null,null,null,null,null,null);
                $unContrat->hydrate($contrat);
                $result[] = $unContrat;
            }
        }
        

        return $result;
    }

    public static function createContrat($idContrat, $idUser, $dateDebut, $dateFin, $typeContrat, $nbHeure){

        $requetePrepa = DBConnex::getInstance()->prepare("insert into contrat (IDCONTRAT, IDUSER, DATEDEBUT, DATEFIN, TYPECONTRAT, NBHEURE) values (:idContrat, :idUser, :dateDebut, :dateFin, :typeContrat, :nbHeure);");
        $requetePrepa->bindParam(":idContrat", $idContrat); 
        $requetePrepa->bindParam(":idUser", $idUser); 
        $requetePrepa->bindParam(":dateDebut", $dateDebut); 
        $requetePrepa->bindParam(":dateFin", $dateFin); 
        $requetePrepa->bindParam(":typeContrat", $typeContrat); 
        $requetePrepa->bindParam(":nbHeure", $nbHeure); 
        $requetePrepa->execute();

        return DBConnex::getInstance()->lastInsertId();
    }


    public static function deleteContrat($idContrat){
        $requetePrepa = DBConnex::getInstance()->prepare("DELETE FROM contrat WHERE IDCONTRAT = :idContrat");
        $requetePrepa->bindParam(":idContrat", $idContrat); 
        $resultat = $requetePrepa->execute();
    
        return $resultat;
    }

    public static function updateContrat($idContrat, $dateDebut, $dateFin, $typeContrat, $nbHeure){
        $requetePrepa = DBConnex::getInstance()->prepare("update contrat set DATEDEBUT = :dateDebut, DATEFIN = :dateFin, TYPECONTRAT = :typeContrat, NBHEURE = :nbHeure WHERE IDCONTRAT = :idContrat;");
        $requetePrepa->bindParam(":idContrat", $idContrat); 
        $requetePrepa->bindParam(":dateDebut", $dateDebut); 
        $requetePrepa->bindParam(":dateFin", $dateFin); 
        $requetePrepa->bindParam(":typeContrat", $typeContrat); 
        $requetePrepa->bindParam(":nbHeure", $nbHeure); 
        $resultat = $requetePrepa->execute();

        return $resultat;
    }

    
}