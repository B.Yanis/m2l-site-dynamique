<?php
//require_once 'modele\dto\utilisateur.php';
class UtilisateurDAO{
        
    public static function verification($login, $mdp){
        
        $requetePrepa = DBConnex::getInstance()->prepare("select * from UTILISATEUR where LOGIN = :login and  mdp = md5(:mdp)");

        
        $requetePrepa->bindParam( ":login", $login );
        $requetePrepa->bindParam( ":mdp" ,  $mdp);        
        $requetePrepa->execute();


        while ($ligne = $requetePrepa->fetch(PDO::FETCH_ASSOC)) {
            $idUser = $ligne['IDUSER'];
            $idLigue = $ligne['IDLIGUE'];
            $idFonct = $ligne['IDFONCT'];
            $idClub = $ligne['IDCLUB'];
            $nom = $ligne['NOM'];
            $prenom = $ligne['PRENOM'];
            $login = $ligne['LOGIN'];
            $mdp = $ligne['MDP'];
            $statut = $ligne['STATUT'];
        }

        $utilisateur = new Utilisateur($idUser, $idLigue, $idFonct, $idClub, $nom, $prenom, $login, $mdp, $statut);

        return $utilisateur;
    }

    public static function allUsers(){
        $result = [];

        $requetePrepa = DBConnex::getInstance()->prepare("select * from UTILISATEUR ");
     
        $requetePrepa->execute();

        $liste = $requetePrepa->fetchAll(PDO::FETCH_ASSOC); 

        if(!empty($liste)){
            foreach($liste as $user){
                $unUser = new Utilisateur(null,null,null,null,null,null,null,null,null);
                $unUser->hydrate($user);
                $result[] = $unUser;
            }
        }

        return $result;
    }

    public static function allSalarié(){

        $result = [];

        $requetePrepa = DBConnex::getInstance()->prepare("select * from UTILISATEUR where STATUT = 'salarié' ;");
     
        $requetePrepa->execute();

        $liste = $requetePrepa->fetchAll(PDO::FETCH_ASSOC); 

        if(!empty($liste)){
            foreach($liste as $user){
                $unUser = new Utilisateur(null,null,null,null,null,null,null,null,null);
                $unUser->hydrate($user);
                $result[] = $unUser;
            }
        }

        return $result;
    }
    

}
?>