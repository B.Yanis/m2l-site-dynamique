<?php
class SalarieDAO{
        
    public static function recuperationContrat($idUser){
        $requetePrepa = DBConnex::getInstance()->prepare("select * from CONTRAT where idUSER = :idUser ;");

        $requetePrepa->bindParam( ":idUser", $idUser );       
        $requetePrepa->execute();
        while ($ligne = $requetePrepa->fetch(PDO::FETCH_ASSOC)) {
            $idContrat = $ligne['IDCONTRAT'];
            $idUser = $ligne['IDUSER'];
            $dateDebut = $ligne['DATEDEBUT'];
            $dateFin = $ligne['DATEFIN'];
            $typeContrat = $ligne['TYPECONTRAT'];
            $nbHeure = $ligne['NBHEURE'];
        }

        $contrat = new Contrat($idContrat, $idUser, $dateDebut, $dateFin, $typeContrat, $nbHeure);

        return $contrat;
    }
    
    public static function recuperationBulletin($idContrat){
        $requetePrepa = DBConnex::getInstance()->prepare("select * from BULLETIN where IDCONTRAT = :idContrat ;");

        $requetePrepa->bindParam( ":idContrat", $idContrat );       
        $requetePrepa->execute();

        while ($ligne = $requetePrepa->fetch(PDO::FETCH_ASSOC)) {
            $idBulletin = $ligne['IDBULLETIN'];
            $idContrat = $ligne['IDCONTRAT'];
            $mois = $ligne['MOIS'];
            $annee = $ligne['ANNEE'];
            $bulletinPDF = $ligne['BULLETINPDF'];
        }

        $contrat = new Bulletin($idBulletin, $idContrat, $mois, $annee, $bulletinPDF);

        return $contrat;

    }
    
}