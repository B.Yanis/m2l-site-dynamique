<?php
class BulletinDAO{

    public static function lesBulletins($idContrat){
        $result = [];
        $requetePrepa = DBConnex::getInstance()->prepare("select * from bulletin where IDCONTRAT = :idContrat ;");

        $requetePrepa->bindParam(":idContrat", $idContrat );       
        $requetePrepa->execute();

        $liste = $requetePrepa->fetchAll(PDO::FETCH_ASSOC); 

        if(!empty($liste)){
            foreach($liste as $bulletin){
                $unBulletin = new Bulletin(null,null,null,null,null);
                $unBulletin->hydrate($bulletin);
                $result[] = $unBulletin;
            }
        }
        

        return $result;

    }

    public static function allBulletin(){
        $result = [];
        $requetePrepa = DBConnex::getInstance()->prepare('select * from bulletin;');

        $requetePrepa->execute();

        $liste = $requetePrepa->fetchAll(PDO::FETCH_ASSOC); 

        if(!empty($liste)){
            foreach($liste as $bulletin){
                $unBulletin = new Bulletin(null,null,null,null,null);
                $unBulletin->hydrate($bulletin);
                $result[] = $unBulletin;
            }
        }
        

        return $result;
    }

    public static function createBulletin($idContrat, $mois, $annee, $pdf){

        $requetePrepa = DBConnex::getInstance()->prepare("insert into bulletin (IDCONTRAT, MOIS, ANNEE, BULLETINPDF) values (:idContrat, :mois, :annee, :pdf);");
        $requetePrepa->bindParam(":idContrat", $idContrat); 
        $requetePrepa->bindParam(":mois", $mois); 
        $requetePrepa->bindParam(":annee", $annee); 
        $requetePrepa->bindParam(":pdf", $pdf);  
        $requetePrepa->execute();

        return DBConnex::getInstance()->lastInsertId();
    }


    public static function deleteBulletin($idBulletin){
        $requetePrepa = DBConnex::getInstance()->prepare("DELETE FROM bulletin WHERE IDBULLETIN = :idBulletin");
        $requetePrepa->bindParam(":idBulletin", $idBulletin); 
        $resultat = $requetePrepa->execute();
    
        return $resultat;
    }

    public static function updateBulletin($idBulletin, $mois, $annee, $pdf){
        $requetePrepa = DBConnex::getInstance()->prepare("update bulletin set MOIS = :mois, ANNEE = :annee, BULLETINPDF = :pdf WHERE IDBULLETIN = :idBulletin;");
        $requetePrepa->bindParam(":idBulletin", $idBulletin); 
        $requetePrepa->bindParam(":mois", $mois); 
        $requetePrepa->bindParam(":annee", $annee); 
        $requetePrepa->bindParam(":pdf", $pdf); 
        $resultat = $requetePrepa->execute();

        return $resultat;
    }
}