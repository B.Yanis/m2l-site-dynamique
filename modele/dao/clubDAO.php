<?php
class ClubDAO{
        
    public static function getClubs($idLigue){
        $result = [];
        $requetePrepa = DBConnex::getInstance()->prepare("select * from club where idligue = :idLigue" );
        
        $requetePrepa->bindParam(":idLigue", $idLigue);
            
        $requetePrepa->execute();
        $resultat = $requetePrepa->fetchAll(PDO::FETCH_ASSOC); 
                
        if(!empty($resultat)){
            foreach($resultat as $club){
                $unClub = new Club(null, null,null,null, null);
                $unClub->hydrate($club);
                $result[] = $unClub;
            }
        }
        return $result;
    }
    public static function getPosition($idCommune){
        $requetePrepa = DBConnex::getInstance()->prepare("select * from commune where IDCOMMUNE = :idCommune" );

        $requetePrepa->bindParam(":idCommune", $idCommune);
            
        $requetePrepa->execute();
        $resultat = $requetePrepa->fetchAll(PDO::FETCH_ASSOC); 

        if(!empty($resultat)){
            foreach($resultat as $position){
                $uneCommune = new Commune(null, null, null, null);
                $uneCommune->hydrate($position);
            }
        }
        return $uneCommune;
        
    }
}